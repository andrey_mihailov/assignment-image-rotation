#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_H
#include "../include/image.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>



enum read_status  {
    READ_OK = 0,
    READ_INVALID_BITS,
    READ_FILE_ERROR,
    READ_INVALID_SIGNATURE

};

enum read_status from_bmp( FILE* file_in, struct image* img );

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR

};

enum write_status to_bmp( FILE* file_out, struct image* img );


#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_H
