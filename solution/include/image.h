//
// Created by Andrew Mihailov on 23.11.2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

struct pixel {
    uint8_t b,g,r;
};

struct image {
    uint32_t width, height;
    struct pixel* data;
};


#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
