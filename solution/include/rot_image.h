//
// Created by Andrew Mihailov on 23.11.2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_ROT_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_ROT_IMAGE_H

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
struct image rotate( struct image const* source );
void free_image(struct image image);
#endif //ASSIGNMENT_IMAGE_ROTATION_ROT_IMAGE_H
