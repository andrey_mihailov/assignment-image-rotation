//
// Created by Andrew Mihailov on 23.11.2022.
//
#include "../include/image.h"
#include "../include/rot_image.h"
#include <stdlib.h>


struct image new_image(uint64_t width, uint64_t height) {
    return (struct image) {
            .width = width,
            .height = height,
            .data = malloc(width * height * sizeof(struct pixel))
    };
}

struct pixel getter(const struct image* image, size_t i, size_t j) {
    return image->data[image->width * (image->height - (j+1)) + i];
}


void setter(struct pixel pixel, struct image* image, size_t i, size_t j) {
    image->data[i * image->width + j] = pixel;
}


struct image rotate( struct image const* source ) {
    struct image image_out = new_image(source->height, source->width);
    for (size_t i = 0; i < source->width; ++i) {
        for (size_t j = 0; j < source->height; ++j) {
            setter(getter(source,i,j), &image_out, i, j);
        }
    }
    return image_out;
}

void free_image(struct image image) {
    free(image.data);
}
