#include "../include/bmp.h"
#include "../include/rot_image.h"
#include <stdio.h>

int main( int argc, char** argv ) {
    if (argc != 3) return 1;

    FILE *image_in = fopen(argv[1],"rb");
    if (!image_in) return 1;
    struct image img;
    FILE* image_out = fopen(argv[2],"wb");
    if (!image_out) return 1;

    if (from_bmp(image_in, &img) != READ_OK) {
        fclose(image_in);
        return 1;
    }

    struct image rotated = rotate(&img);

    if (to_bmp(image_out, &rotated) != WRITE_OK) {
        fclose(image_out);
        return 1;
    }
    free_image(img);
    free_image(rotated);
    fclose(image_in);
    fclose(image_out);

    return 0;
}
