//
// Created by Andrew Mihailov on 23.11.2022.
//

#include "../include/bmp.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>


struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
static uint32_t get_padding(uint32_t width) {
    if (width % 4 != 0) {
        return 4 - (width*3) % 4;
    }
    return 0;
}

static struct bmp_header new_header(const struct image* img) {
    const uint32_t size_img = img->width * img->height * sizeof(struct pixel);
    struct bmp_header new_h;
    new_h.bfType = 0x4D42,
    new_h.bfileSize = sizeof(struct bmp_header) + size_img + get_padding(img->width)*img->height,
    new_h.bfReserved = 0,
    new_h.bOffBits = sizeof(struct bmp_header),
    new_h.biSize = 40,
    new_h.biWidth = img->width,
    new_h.biHeight = img->height,
    new_h.biPlanes = 1,
    new_h.biBitCount = 24,
    new_h.biCompression = 0,
    new_h.biSizeImage = size_img,
    new_h.biXPelsPerMeter = 0,
    new_h.biYPelsPerMeter = 0,
    new_h.biClrUsed = 0,
    new_h.biClrImportant = 0;

    return new_h;
}


enum read_status from_bmp( FILE* file_in, struct image* img ) {
    if (!file_in) return READ_FILE_ERROR;
    struct bmp_header new_h;
    if (fread(&new_h, sizeof(struct bmp_header), 1, file_in) != 1) return READ_FILE_ERROR;
    if (new_h.bfType != 0x4D42) return READ_INVALID_SIGNATURE;
    if (new_h.biBitCount != 24) return READ_INVALID_BITS;
    img->width = new_h.biWidth;
    img->height = new_h.biHeight;
    struct pixel* data = malloc(img->width * img->height * sizeof(struct pixel));
    uint8_t padding = get_padding(img->width);
    for (size_t i = 0; i < img->height; ++i) {
        if(fread(data + i * img->width, sizeof(struct pixel), img->width, file_in)!=img->width|| fseek(file_in, padding, SEEK_CUR)!=0)return READ_FILE_ERROR;
    }
    img->data = data;
    return READ_OK;
}

enum write_status to_bmp( FILE* file_out, struct image* img ) {
    if (!file_out) return WRITE_ERROR;
    struct bmp_header new_h = new_header(img);
    fwrite(&new_h, sizeof(struct bmp_header), 1, file_out);
    const uint32_t height = img->height;
    const uint32_t width = img->width;
    uint8_t padding = get_padding(width);
    uint8_t const* buffer[3] = {0};
    for (size_t i = 0; i < height; ++i) {
        if(fwrite(img->data + i * width, sizeof(struct pixel)*width, 1, file_out)!=1)return WRITE_ERROR;
        if (padding != 0) {
            if(fwrite(buffer, padding, 1, file_out)!=1)return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}
